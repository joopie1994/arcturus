# **Arcturus Emulator is released under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.txt). Modifications are allowed but must disclosed. Changes in the about menu are forbidden as they are part of the license.** #

# Arcturus Emulator #

## **To Get The Camera To Work Visit http://arcturus.wf and register.** ##
## **TUTORIAL FOR PLUGINS http://arcturus.wf/thread-2415.html** ##
# **DO NOT EDIT THE SOURCE. USE THE PLUGIN API.** #
## Current Stable Version: 1.1.0 ##
Arcturus Emulator is a Habbo emulator written in Java aiming to be an exact clone of the offical server.

Targeting PRODUCTION-201601012205-226667486

SWF: http://arcturus.wf/mirrors/PRODUCTION-201601012205-226667486.swf

SCRIPTS: http://arcturus.wf/mirrors/PRODUCTION-201601012205-226667486_scripts.txt

# **Compiled version (Including Database): http://arcturus.wf/mirrors/ArcturusEmulator_1.1.0.zip** #

Latest Compiled Version: http://arcturus.wf:8080

Older Files: http://arcturus.wf/mirrors/

JavaDoc: http://arcturus.wf/doc/
### Contributing ###

Anyone is allowed to contribute to the project. Reporting bugs and issues goes via the issue tracker.

You can request features in the issue tracker.

### Plugins ###
An example plugin can be found here: https://bitbucket.org/Wesley12312/pluginexample

Here is a list of cool plugins you can download:

* https://bitbucket.org/Wesley12312/kickwars-plugin

If you want the link to your plugin repository to be added, create an issue.

### Contact ###

* http://arcturus.wf/ | Main forums.